//
//  Level.swift
//  CookieCrunch
//
//  Created by Matt on 10/13/14.
//  Copyright (c) 2014 MattWilliams. All rights reserved.
//

import Foundation

let NumColumns = 9;
let NumRows = 9;

class Level {
    private var cookies = Array2D<Cookie>(columns: NumColumns, rows: NumRows);
    // 1
    init(filename: String) {
        if let dictionary = Dictionary<String,AnyObject>.loadJSONFromBundle(filename) {
            // 2
            if let tilesArray: AnyObject = dictionary["tiles"] {
                // 3
                for (row, rowArray) in enumerate(tilesArray as [[Int]]) {
                    // 4
                    let tileRow = NumRows - row - 1;
                    
                    // 5
                    for (column, value) in enumerate(rowArray) {
                        if value == 1 {
                            tiles [column, tileRow] = Tile();
                        }
                    }
                }
            }
        }
    }
    
    func cookieAtColumn(column: Int, row: Int) -> Cookie? {
        
        assert(column >= 0 && column < NumColumns);
        assert(row >= 0 && row < NumRows);
        
        return cookies[column, row];
    }
        
    func shuffle() -> Set<Cookie> {
        return createInitialCookies();
        }
    
    private func createInitialCookies() -> Set<Cookie> {
        var set = Set<Cookie>();
        
        // 1
        for row in 0..<NumRows {
            for column in 0..<NumColumns {
                
                if tiles[column, row] != nil {
                    
                    // 2
                    var cookieType = CookieType.random();
                    
                    // 3
                    let cookie = Cookie(column: column, row: row, cookieType: cookieType);
                    cookies[column, row] = cookie;
                    
                    // 4
                    set.addElement(cookie);
                }
            }
        }
        
        
        return set;
    }
    
    private var tiles = Array2D<Tile>(columns: NumColumns, rows: NumRows);
    
    func tileAtColumn(column: Int, row: Int) -> Tile? {
        assert(column >= 0 && column < NumColumns);
        assert(row >= 0 && row < NumRows);
        
        return tiles[column, row];
    }
}

